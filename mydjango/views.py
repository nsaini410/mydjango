from django.shortcuts import render
from .forms import InventarioForm
from .models import Inventario

# Create your views here.
def index(request):
    user_name = request.GET.get('name', 'sin parametro')
    form = InventarioForm()
    if request.method == 'POST':
        form = InventarioForm(data=request.POST)
        if form.is_valid():
            form.save()
    inventario_list = Inventario.objects.all()
    return render(request,
                  'base.html',
                  {'user_name': user_name,
                   'form': form,
                   'lista' : inventario_list})