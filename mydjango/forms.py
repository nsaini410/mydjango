from django.forms import ModelForm
from .models import Inventario


class InventarioForm(ModelForm):

    class Meta:
        model = Inventario
        fields = ['nombre', 'proveedor']